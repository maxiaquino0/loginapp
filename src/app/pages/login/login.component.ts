import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UsuarioModel } from '../../models/usuario.model';
import { AuthService } from 'src/app/services/auth.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: UsuarioModel;
  record = false;

  constructor(private auth: AuthService,
              private router: Router
    ) { }

  ngOnInit() {
    this.usuario = new UsuarioModel();

    if (localStorage.getItem('email')) {
      this.usuario.email = localStorage.getItem('email');
      this.record = true;
    }
  }

  login(f: NgForm) {
    if (f.invalid) {
      return;
    }

    Swal.fire({allowOutsideClick: false, icon: 'info', text: 'Espere por favor...'});
    Swal.showLoading();
    this.auth.login(this.usuario).subscribe(
      resp => {
        console.log(resp);
        Swal.close();
        if (this.record) {
          localStorage.setItem('email', this.usuario.email);
        }
        this.router.navigateByUrl('/home');
      },
      (err) => {
        console.log(err.error.error.message);
        Swal.fire({title: 'Error al autenticar', icon: 'error', text: err.error.error.message});
      }
    );
  }

}
