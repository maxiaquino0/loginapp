# Seccion 10 - LoginApp
El objetivo de la sección, es trabajar con un proceso de autenticación por token tradicional, veremos temas como:
----------------------------------------------
* Validar formularios
* Tokens
* LocalStorage
* Borrar tokens
* Caducidad de tokens
* Creación de usuarios
* Posteos
* Firebase REST API

## Recursos
https://sweetalert2.github.io/

https://github.com/sweetalert2/ngx-sweetalert2#package-installation--usage

# LoginApp

Cascaron de un login que usaremos en la sección 10 de mi curso de Angular de cero a experto.

https://www.udemy.com/angular-2-fernando-herrera/?couponCode=ANGULAR-10


![](https://github.com/Klerith/angular-login-demoapp/blob/master/src/assets/images/demo.png?raw=true)
